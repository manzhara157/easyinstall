#!/bin/bash

path="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
user="$( cut -d: -f1,3 /etc/passwd | egrep ':[0-9]{4}$' | cut -d: -f1 )"
os="$(cat /etc/os-release | egrep ID | head -1)"

if [ "$os" == "ID=arch" ]
then
	sudo pacman -S unzip wget inotify-tools
else
	sudo apt install unzip wget inotify-tools -y
fi

sudo apt install unzip wget inotify-tools -y
mkdir .local/share/icons
mkdir .local/share/applications
#mkdir .local/share/applications/appDesktop
mkdir AppImage
cd AppImage
mkdir .appDesktop
wget https://gitlab.com/manzhara157/easyinstall/-/archive/master/easyinstall-master.zip
unzip easyinstall-master.zip
cd easyinstall-master
cp .AppInstall ../
cp .EasyInstall.sh ../
cp .appIcon/* $HOME/.local/share/icons
cd ..
cp .AppInstall ../
cp .EasyInstall.sh ../
rm -r easyinstall-master
rm easyinstall-master.zip

#echo "#!/bin/bash" > rc.local
#echo "su -c '/home/$user/AppImage/.EasyInstall.sh' #$user" >> rc.local
#echo "exit 0" >> rc.local
#sudo cp rc.local /etc/
#sudo chmod +x /etc/rc.local
#rm rc.local

#path=$(pwd)
cat << EOF >> /home/$user/.profile
# Start EasyInstall AppImage files
if [ -f $path/AppImage/.EasyInstall.sh ]; then
	. $path/AppImage/.EasyInstall.sh &
fi
EOF
