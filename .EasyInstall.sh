#!/bin/bash
dir1="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
echo $dir1
while inotifywait -qqre modify "$dir1"; do
	sleep 1
	$dir1/.AppInstall
    echo Changed!
done
