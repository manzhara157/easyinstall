## What is EasyInstall?

EasyInstall is a script for quickly forming icons in the menu for AppImage files. You can say to quickly "install" AppImage files.

## How to use?

After installation, a special AppImage folder will be created. You need to copy the AppImage file to this folder. And that's all.

Note: The file must have an .AppImage extension. With capital letters! Before copying, .appimage must be renamed to .AppImage.

## How to Install

To install EasyInstall:

1. Download **Install.sh** to your Home folder.
2. Make **Install.sh** executable. ```sudo chmod +x Install.sh```
3. Run script **Install.sh** as user.
4. Logout and login, to start script automatically.

Note: Install script add line to .profile file. If you run the script several times, you may write several equal lines in .profile file. In WM this method may not work.
